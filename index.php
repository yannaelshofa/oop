<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");
$sheep = new animal("shaun");

echo "Name : " .$sheep->name ."<br>"; // "shaun"
echo "legs : " .$sheep->legs."<br>"; // 4
echo "Cold blooded : " .$sheep->cold_blooded . "<br>"; // "no"
/////
$kodok = new frog("buduk");
echo "Name : " .$kodok->name ."<br>"; // "shaun"
echo "legs : " .$kodok->legs."<br>"; // 4
echo "Cold blooded : " .$kodok->cold_blooded . "<br>"; // "no"
echo "Jump: ";
$kodok->jump() ; // "hop hop"
echo "<br>";
// index.php
$sungokong = new Ape("kera sakti");
echo "Name : " .$sungokong->name ."<br>"; // "shaun"
echo "legs : " .$sungokong->legs."<br>"; // 4
echo "Cold blooded : " .$sungokong->cold_blooded . "<br>"; // "no"
echo "Yell: ";
$sungokong->yell() ;// "Auooo";
echo "<br>";